package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	port := os.Getenv("SERVICE_PORT")
	if port == "" {
		port = "9000"
	}

	host := os.Getenv("SERVICE_HOST")
	if host == "" {
		host = "1.1.1.1"
	}
	addr := host + ":" + port
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})

	if err := r.Run(addr); err != nil {
		log.Fatal(err)
	}
}
