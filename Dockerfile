# syntax=docker/dockerfile:1

FROM golang:1.21-alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .

RUN go build -o service ./cmd/main.go

ARG PORT=3000
EXPOSE $PORT 

CMD [ "/app/service" ]

