up:
	@docker compose up --build
.PHONY: up

down:
	@docker compose down
.PHONY: down

